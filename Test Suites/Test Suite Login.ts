<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Test Suite Login</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>c5e762ef-0513-4b63-8499-3f54b7046e8c</testSuiteGuid>
   <testCaseLink>
      <guid>8e81f297-418a-4058-a8f3-2fb3a119aa03</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Login/TC_LOGIN_05</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>a51bf15c-24bf-4b30-904d-89c4254ad202</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Data Login Cura</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>a51bf15c-24bf-4b30-904d-89c4254ad202</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>3e4cedf0-3b1e-4484-a07e-2bd6c2ff8a16</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>a51bf15c-24bf-4b30-904d-89c4254ad202</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>2033e5eb-207f-4bfc-bd0e-b635afe873b8</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
